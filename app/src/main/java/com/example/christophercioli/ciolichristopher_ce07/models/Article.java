// Christopher Cioli
// JAV2 - 1805
// Article.java

package com.example.christophercioli.ciolichristopher_ce07.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Article implements Serializable {

    // Member Variables
    private final byte[] mImageBytes;
    private final String mHeadline;
    private final String mDescription;
    private final String mSourceUrl;
    private final String mImageUrl;

    // Constructors
    private Article(String _headline, String _description, String _sourceUrl, String _imageUrl) {
        mImageBytes = null;
        mHeadline = _headline;
        mDescription = _description;
        mSourceUrl = _sourceUrl;
        mImageUrl = _imageUrl;
    }

    // Getters
    public byte[] getImageBytes() {
        return mImageBytes;
    }

    public String getHeadline() {
        return mHeadline;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getSourceUrl() {
        return mSourceUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    // JSON Parse Methods
    public static ArrayList<Article> parseArticlesFromJson(String _inputJson) throws JSONException {
        ArrayList<Article> articles = new ArrayList<>();

        JSONObject topLevelObject = new JSONObject(_inputJson);
        JSONArray articlesArray = topLevelObject.getJSONArray("articles");
        for (int i = 0; i < articlesArray.length(); i++) {
            JSONObject article = articlesArray.getJSONObject(i);

            String headline = article.getString("title");
            String description = article.getString("description");
            String sourceUrl = article.getString("url");
            String imageUrl = "";
            if (article.getString("urlToImage") != null) {
                imageUrl = article.getString("urlToImage");
            }

            articles.add(new Article(headline, description, sourceUrl, imageUrl));
        }

        return articles;
    }
}
