// Christopher Cioli
// JAV2 - 1805
// NewsListFragment.java

package com.example.christophercioli.ciolichristopher_ce07.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.christophercioli.ciolichristopher_ce07.R;
import com.example.christophercioli.ciolichristopher_ce07.adapters.ArticleAdapter;
import com.example.christophercioli.ciolichristopher_ce07.models.Article;

import java.util.ArrayList;

public class NewsListFragment extends ListFragment {

    // Member Variables
    private ArrayList<Article> mArticles;

    // Constants
    private static final String ARG_ARTICLES = "ARG_ARTICLES";

    // Default Constructor
    public NewsListFragment() { }

    public static NewsListFragment newInstance(ArrayList<Article> _articles) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_ARTICLES, _articles);

        NewsListFragment fragment = new NewsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news_list, container, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            mArticles = (ArrayList<Article>) getArguments().getSerializable(ARG_ARTICLES);
        }

        setUpUIElements();
    }

    private void setUpUIElements() {
        if (getView() != null) {
            ListView mNewsListView = getView().findViewById(android.R.id.list);

            mNewsListView.setAdapter(new ArticleAdapter(mArticles, getContext()));
            mNewsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Article a = mArticles.get(i);
                    Intent viewWebArticleIntent = new Intent(Intent.ACTION_VIEW);
                    viewWebArticleIntent.setData(Uri.parse(a.getSourceUrl()));
                    startActivity(viewWebArticleIntent);
                }
            });
        }
    }
}
