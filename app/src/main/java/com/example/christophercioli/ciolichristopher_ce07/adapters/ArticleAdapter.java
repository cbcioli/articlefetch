// Christopher Cioli
// JAV2 - 1805
// ArticleAdapter.java

package com.example.christophercioli.ciolichristopher_ce07.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.christophercioli.ciolichristopher_ce07.R;
import com.example.christophercioli.ciolichristopher_ce07.models.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ArticleAdapter extends BaseAdapter {

    // Member Variables
    private final ArrayList<Article> mArticles;
    private final Context mContext;

    public ArticleAdapter(ArrayList<Article> _articles, Context _context) {
        mArticles = _articles;
        mContext = _context;
    }

    @Override
    public int getCount() {
        if (mArticles != null) {
            return mArticles.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        if (mArticles != null) {
            return mArticles.get(i);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return 0x1110 + i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder vh;

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.article_cell, viewGroup,
                    false);
            vh = new ViewHolder(view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) view.getTag();
        }

        Article a = (Article) getItem(i);

        if (a != null) {
            Picasso.get().load(a.getImageUrl()).into(vh.mArticleImageView);
            vh.mHeadlineTextView.setText(a.getHeadline());
            vh.mDescriptionTextView.setText(a.getDescription());
        }

        return view;
    }

    static class ViewHolder {
        private final ImageView mArticleImageView;
        private final TextView mHeadlineTextView;
        private final TextView mDescriptionTextView;

        ViewHolder(View _layout) {
            mArticleImageView = _layout.findViewById(R.id.articleImageView);
            mHeadlineTextView = _layout.findViewById(R.id.headlineTextView);
            mDescriptionTextView = _layout.findViewById(R.id.descriptionTextView);
        }
    }
}
