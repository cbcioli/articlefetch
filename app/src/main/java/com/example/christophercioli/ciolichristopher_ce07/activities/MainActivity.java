// Christopher Cioli
// JAV2 - 1805
// MainActivity.java

package com.example.christophercioli.ciolichristopher_ce07.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.christophercioli.ciolichristopher_ce07.R;
import com.example.christophercioli.ciolichristopher_ce07.fragments.NewsListFragment;
import com.example.christophercioli.ciolichristopher_ce07.models.Article;
import com.example.christophercioli.ciolichristopher_ce07.services.DownloadNewsIntentService;
import com.example.christophercioli.ciolichristopher_ce07.services.SaveArticleIntentService;
import com.example.christophercioli.ciolichristopher_ce07.utilities.FileStorageUtility;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SaveArticleReceiver downloadReceiver = new SaveArticleReceiver();
        IntentFilter filter = new IntentFilter(
                SaveArticleIntentService.SAVE_ARTICLE_BROADCAST_ACTION);
        registerReceiver(downloadReceiver, filter);

        ArrayList<Article> articles = FileStorageUtility.getStoredArticles(this);

        setAlarm(this);
        refreshNewsListView(articles);
    }

    private void refreshNewsListView(ArrayList<Article> _articles) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.newsFrameLayout, NewsListFragment.newInstance(_articles))
                .commit();
    }

    // Broadcast Receivers
    public class SaveArticleReceiver extends BroadcastReceiver {

        public SaveArticleReceiver() { }

        // BroadcastReceiver Methods
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, R.string.articleSaved, Toast.LENGTH_SHORT).show();
            refreshNewsListView(FileStorageUtility.getStoredArticles(MainActivity.this));
        }
    }

    public static class FireDownloadNewsServiceReceiver extends BroadcastReceiver {

        public FireDownloadNewsServiceReceiver() { }

        // BroadcastReceiver Methods
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null) {
                // Check to ensure that the action hasn't been spoofed
                if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                    setAlarm(context);
                }
            }
        }
    }

    private static void setAlarm(Context c) {
        Intent startDownloadIntent = new Intent(c, DownloadNewsIntentService.class);
        PendingIntent downloadPendingIntent = PendingIntent.getService(c, 0,
                startDownloadIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager mgr = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
        if (mgr != null) {
            mgr.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(),
                    60000, downloadPendingIntent);
        }
    }
}
