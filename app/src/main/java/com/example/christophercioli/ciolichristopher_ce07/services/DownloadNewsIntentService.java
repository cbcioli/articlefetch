// Christopher Cioli
// JAV2 - 1805
// DownloadNewsIntentService.java

package com.example.christophercioli.ciolichristopher_ce07.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import com.example.christophercioli.ciolichristopher_ce07.R;
import com.example.christophercioli.ciolichristopher_ce07.models.Article;
import com.example.christophercioli.ciolichristopher_ce07.utilities.NetworkUtility;
import org.json.JSONException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class DownloadNewsIntentService extends IntentService {

    // Constants
    private static final String SERVICE_NAME = "DownloadNewsIntentService";
    private static final String NEWS_URL= "https://newsapi.org/v2/top-headlines?country=us&apiKey=";
    private static final String API_KEY = "58f68a3a55ca48be886cd923c3f3792a";
    private static final String NOTIFICATION_CHANNEL =
            "com.example.christophercioli.ciolichristopher_ce07.ARTICLE_FETCH_CHANNEL";
    private static final int ARTICLE_NOTIFICATION_ID = 52618;
    public static final String ARTICLE_EXTRA = "ARTICLE_EXTRA";

    public DownloadNewsIntentService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (NetworkUtility.isConnected(this)) {
            try {
                String inboundJson = NetworkUtility.getNetworkData(NEWS_URL + API_KEY);
                ArrayList<Article> articles = Article.parseArticlesFromJson(inboundJson);

                Article randomArticle = selectRandomArticle(articles);
                buildAndSendNotification(randomArticle);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private Article selectRandomArticle(ArrayList<Article> articles) {
        Random r = new Random();
        int random = r.nextInt(articles.size());
        return articles.get(random);
    }

    private void buildAndSendNotification(Article a) {
        Intent viewWebArticleIntent = new Intent(Intent.ACTION_VIEW);
        viewWebArticleIntent.setData(Uri.parse(a.getSourceUrl()));
        PendingIntent webPendingIntent = PendingIntent.getActivity(this, 0,
                viewWebArticleIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent saveArticleIntent = new Intent (this, SaveArticleIntentService.class);
        saveArticleIntent.putExtra(ARTICLE_EXTRA, a);
        PendingIntent saveArticlePendingIntent = PendingIntent.getService(this,
                0, saveArticleIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,
                NOTIFICATION_CHANNEL);
        builder.setContentTitle(getString(R.string.newArticleFound))
                .setSmallIcon(R.drawable.ic_article_white_24dp)
                .setLargeIcon(a.getImageBytes() != null ?
                        BitmapFactory.decodeByteArray(a.getImageBytes(), 0, 0,
                                options) : BitmapFactory.decodeResource(getResources(),
                        R.drawable.ic_note_blue_36dp))
                .setContentText(a.getHeadline())
                .setAutoCancel(true);

        NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
        bigStyle.setBigContentTitle(a.getHeadline())
                .setSummaryText(getString(R.string.newArticleFound))
                .bigText(a.getDescription());

        builder.setContentIntent(webPendingIntent)
                .setStyle(bigStyle)
                .addAction(android.R.drawable.ic_menu_save, "Save", saveArticlePendingIntent)
                .setColor(this.getResources().getColor(R.color.colorPrimary));

        NotificationManager mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (mgr != null) {
            mgr.notify(ARTICLE_NOTIFICATION_ID, builder.build());
        }
    }
}
