// Christopher Cioli
// JAV2 - 1805
//

package com.example.christophercioli.ciolichristopher_ce07.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkUtility {

    public static boolean isConnected(Context _context) {

        ConnectivityManager mgr = (ConnectivityManager)_context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(mgr != null) {
            NetworkInfo info = mgr.getActiveNetworkInfo();

            if(info != null) {
                return info.isConnected();
            }
        }

        return false;
    }

    public static String getNetworkData(String _url) throws IOException {
        String data = null;
        InputStream is = null;
        HttpURLConnection connection = null;

        try {
            URL url = new URL(_url);

            connection = (HttpURLConnection)url.openConnection();

            connection.connect();

            is = connection.getInputStream();

            data = IOUtils.toString(is, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }  finally {
            if (connection != null && is != null) {

                is.close();

                connection.disconnect();
            }
        }

        return data;
    }
}
