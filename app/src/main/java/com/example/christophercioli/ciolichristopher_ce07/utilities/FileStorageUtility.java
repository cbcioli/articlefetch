// Christopher Cioli
// JAV2 - 1805
// FileStorageUtility.java

package com.example.christophercioli.ciolichristopher_ce07.utilities;

import android.content.Context;

import com.example.christophercioli.ciolichristopher_ce07.models.Article;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;

public class FileStorageUtility {

    // Constants
    private static final String ARTICLE_STORAGE_LOCATION = "articles/";

    public static void storeArticle(Context c, Article a) {
        File protectedStorage = c.getExternalFilesDir(ARTICLE_STORAGE_LOCATION);
        Date currentDate = new Date();
        String filename = "/article_" + currentDate.toString();

        if (protectedStorage != null) {
            File newFile = new File(protectedStorage.getAbsolutePath() + filename);

            try {
                if (newFile.createNewFile()) {
                    FileOutputStream fos = new FileOutputStream(newFile);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(a);

                    oos.close();
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static ArrayList<Article> getStoredArticles(Context c) {
        ArrayList<Article> outArticles = new ArrayList<>();
        File protectedStorage = c.getExternalFilesDir(ARTICLE_STORAGE_LOCATION);

        if (protectedStorage != null) {
            File[] storedArticles = protectedStorage.listFiles();

            for (File f : storedArticles) {
                Article article = null;
                try {
                    FileInputStream fis = new FileInputStream(f);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    article = (Article) ois.readObject();
                    ois.close();
                    fis.close();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }

                if (article != null) {
                    outArticles.add(article);
                }
            }
        }

        return outArticles;
    }
}
