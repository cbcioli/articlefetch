// Christopher Cioli
// JAV2 - 1805
// SaveArticleIntentService.java

package com.example.christophercioli.ciolichristopher_ce07.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.example.christophercioli.ciolichristopher_ce07.models.Article;
import com.example.christophercioli.ciolichristopher_ce07.utilities.FileStorageUtility;

public class SaveArticleIntentService extends IntentService {

    // Constants
    private static final String SERVICE_NAME = "SaveArticleIntentService";
    public static final String SAVE_ARTICLE_BROADCAST_ACTION =
            "com.example.christophercioli.ciolichristopher_ce07.SAVE_ARTICLE";

    public SaveArticleIntentService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            Article savedArticle = (Article) intent
                    .getSerializableExtra(DownloadNewsIntentService.ARTICLE_EXTRA);
            FileStorageUtility.storeArticle(this, savedArticle);

            Intent articleSavedIntent = new Intent();
            articleSavedIntent.setAction(SAVE_ARTICLE_BROADCAST_ACTION);
            sendBroadcast(articleSavedIntent);
        }
    }
}
